(function () {
    'use strict';
var app = angular.module('browseMoviesAppApp',[]);

app.controller('movieController', function($scope, $http){

//load collection of movies 
fetch();


function fetch(){
	$scope.showresult=true;
	var base = 'http://api.themoviedb.org/3';
    var mode = '/collection/528';
    var apiKey = '9bd1492572feb7a66906839c43595167';
    var callback = 'JSON_CALLBACK'; 
    var url = base + mode + '?api_key=' + apiKey + '&callback=' + callback;
    $scope.result = '';
    //$http service to fetch data from themoviedb api
    $http.jsonp(url)
    .then(function(data, status) { 
      $scope.result = data; 
      console.log($scope.result);
    },function(data, status) {
      $scope.result = data;
       console.log($scope.result);
    });
}

//Click to display detail of a movie along with credits
$scope.showDetail = function(movieId){
    var text=[];
    var castingText=[];
	var creditDetail='';
	var base = 'http://api.themoviedb.org/3';
    var mode = '/movie/' + movieId;
    var modeCredit = '/movie/' + movieId + '/credits';
    var apiKey = '9bd1492572feb7a66906839c43595167';
    var callback = 'JSON_CALLBACK'; 
    var url = base + mode + '?api_key=' + apiKey + '&callback=' + callback;
    var urlcredit = base + modeCredit + '?api_key=' + apiKey + '&callback=' + callback;
    $scope.showDetailPage = true;
	$scope.director='';
    $scope.details = '';
    $scope.credits = '';
    $scope.write='';

    $http.jsonp(url)
    .then(function(data, status) { 
      $scope.details = data; 
    },function(data, status) {
      $scope.details = data;
    });

    $http.jsonp(urlcredit)
    .then(function(data, status) { 
      	$scope.credits = data;
      	creditDetail=$scope.credits;

      	//loop through object to get director, writer 
      	for(var value in creditDetail.data.crew){
    	if(creditDetail.data.crew[value].job === "Director"){
           	$scope.director= creditDetail.data.crew[value].name;
    	}

    	if(creditDetail.data.crew[value].job === "Writer"){
    		text.push(" " + creditDetail.data.crew[value].name);
           	$scope.write= text.join(',');
    	}
    }
    
    //loop through object to get star name
    for(var value in creditDetail.data.cast){
    	castingText.push(" " + creditDetail.data.cast[value].name);
        $scope.star= castingText.join(',');
    }
    },function(data, status) {  //show status if data not found
      $scope.credits = data;
      creditDetail=$scope.credits;
    });

}

// display full size image
$scope.displayImg = function(img){
  $scope.showImg= true;
  $scope.img_path=img;
}

});
})();
